using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    public static void Move(GameObject obj, float dirX, float dirY)
    {
        Rigidbody2D rigidBody = obj.GetComponent<Rigidbody2D>();
        rigidBody.velocity = new Vector2(dirX, dirY);
    }

    public static void Patrol(GameObject obj)
    {
        Enemy enemy = obj.GetComponent<Enemy>();
        Vector2 position = new Vector2 (obj.transform.position.x, obj.transform.position.y -0.25f);
        float beamWall = 1f;
        float beamFloor = 1.5f;

        //Debug.DrawRay(new Vector2(position.x, position.y - 0.5f), new Vector2(1f, -1f), Color.red);
        //Debug.DrawRay(new Vector2(position.x, position.y - 0.5f), new Vector2(1f, 0f), Color.green);

        if (Physics2D.Raycast(position, new Vector2(1f, -1f), beamFloor, 1 << 3).collider != null && Physics2D.Raycast(position, new Vector2(1f, 0f), beamWall, 1 << 3).collider == null && enemy.facing == "right")
        {
            Move(obj, enemy.speed, obj.transform.position.y);
        }
        else
        {
            enemy.facing = "left";
        }

        //Debug.DrawRay(position, new Vector2(-1f, -1f), Color.blue);
        //Debug.DrawRay(position, new Vector2(-1f, 0f), Color.white);

        if (Physics2D.Raycast(position, new Vector2(-1f, -1f), beamFloor, 1 << 3).collider != null && Physics2D.Raycast(position, new Vector2(-1f, 0f), beamWall, 1 << 3).collider == null && enemy.facing == "left")
        {
            Move(obj, -enemy.speed, obj.transform.position.y);
        }
        else
        {
            enemy.facing = "right";
        }
    }

    public static void Follow(GameObject follower, GameObject objFollowed)
    {
        Enemy enemy = follower.GetComponent<Enemy>();
        if (enemy.collide != objFollowed.name)
        {
            follower.transform.position = Vector2.MoveTowards(follower.transform.position, objFollowed.transform.position, enemy.followSpeed * Time.deltaTime);
        }
    }
}
