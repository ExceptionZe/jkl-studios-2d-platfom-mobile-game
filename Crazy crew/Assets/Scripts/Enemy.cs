using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public Animator animator;
    public string facing;
    public float speed = 2f;
    public float followSpeed = 4f;
    public string collide;
    public int maxHealth = 100;
    private int currentHealth;

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name == "Player")
        {
            collide = "Player";
        }
    }

    public void OnCollisionExit2D(Collision2D collision)
    {
        collide = "";
    }

    // Start is called before the first frame update
    void Start()
    {
        currentHealth = maxHealth;
        //ignore traps
        Physics2D.IgnoreLayerCollision(7, 8);

        facing = "left";
        collide = "";
    }

    // Update is called once per frame
    void Update()
    {
        if (Vector2.Distance(this.gameObject.transform.position, GameObject.Find("Player").transform.position) < 5)
        {
            Movement.Follow(this.gameObject, GameObject.Find("Player"));
        }

        else
        {
            Movement.Patrol(this.gameObject);
        }
    }

    public void TakeDamage(int damage)
    {
        currentHealth -= damage;

        animator.SetTrigger("getHit");

        if (currentHealth <= 0)
        {
            Die();
        }
    }

    private void Die()
    {
        animator.SetBool("isDead", true);
    }

    private void Destroy()
    {
        
        this.gameObject.SetActive(false);
        this.enabled = false;
    }
}