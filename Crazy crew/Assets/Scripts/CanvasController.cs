using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CanvasController : MonoBehaviour
{
    Canvas canvas;
    Player player;
    void Start()
    {
        player = GameObject.Find("Player").GetComponent<Player>();
    }

    public void TextUpdate(float valueCoins, float valuePotions)
    {
        canvas = GameObject.Find("Canvas").GetComponent<Canvas>();
        GameObject textCollection = canvas.transform.Find("coinsStat").gameObject;
        TMP_Text coinsStatInput = textCollection.GetComponent<TMP_Text>();
        textCollection = canvas.transform.Find("potionsStat").gameObject;
        TMP_Text potionsStatInput = textCollection.GetComponent<TMP_Text>();
        coinsStatInput.text = valueCoins.ToString();
        potionsStatInput.text = valuePotions.ToString();
    }
}