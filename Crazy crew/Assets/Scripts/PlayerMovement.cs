using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour
{
    private Rigidbody2D rigidBody;
    private Animator anim;
    private SpriteRenderer sprite;
    private BoxCollider2D collider;

    private bool moveLeft;
    private bool moveRight;
    private bool jump = false;
    private bool climb = false;
    private bool isGrounded = false;
    private bool isOnEnemyHead = false;
    private bool collidesWithLadder = false;

    private float horizontalMove = 0f;
    private float verticalMove = 0f;
    [SerializeField] private float speed = 6f;
    [SerializeField] private float jumpForce = 12f;

    private enum MovementState { idle, running, jumping, falling };
    private MovementState state;

    private void Start()
    {
        rigidBody = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        sprite = GetComponent<SpriteRenderer>();
        collider = GetComponent<BoxCollider2D>();

        moveLeft = false;
        moveRight = false;
    }

    private void Update()
    {
        isGrounded = Physics2D.OverlapCircle(new Vector2(transform.position.x, transform.position.y - 1.25f), 0.2f, 1 << 3);
        isOnEnemyHead = Physics2D.OverlapCircle(new Vector2(transform.position.x, transform.position.y - 1.25f), 0.3f, 1 << 7);
        MovePlayer();
        SetMovementState();
        UpdateAnimationState();
    }

    private void FixedUpdate()
    {
        rigidBody.velocity = new Vector2(horizontalMove, verticalMove);
    }

    private void MovePlayer()
    {
        if (moveLeft)
        {
            horizontalMove = -speed;
        }
        else if (moveRight)
        {
            horizontalMove = speed;
        }
        else
        {
            horizontalMove = rigidBody.velocity.x;
            rigidBody.velocity = new Vector2(0.0f, rigidBody.velocity.y);
        }

        if (collider.CompareTag("Ladder"))
        {
            collidesWithLadder = true;
        }
        else
        {
            collidesWithLadder = false;
        }

        verticalMove = rigidBody.velocity.y;
    }

    private void SetMovementState()
    {
        if (rigidBody.velocity.y > .1f)
        {
            state = MovementState.jumping;
        }
        else if (rigidBody.velocity.y < -.1f)
        {
            state = MovementState.falling;
        }
        else if (isGrounded || isOnEnemyHead)
        {
            if (Mathf.Abs(rigidBody.velocity.x) > 0.1f)
            {
                if (rigidBody.velocity.x < 0)
                {
                    sprite.flipX = true;
                }
                else if (rigidBody.velocity.x > 0)
                {
                    sprite.flipX = false;
                }
                state = MovementState.running;
            }
            else
            {
                state = MovementState.idle;
            }
        }
    }

    private void UpdateAnimationState()
    {
        anim.SetInteger("state", (int)state);
    }

    public void PointerDownLeft()
    {
        moveLeft = true;
        sprite.flipX = true;
    }

    public void PointerUpLeft()
    {
        moveLeft = false;
    }

    public void PointerDownRight()
    {
        moveRight = true;
        sprite.flipX = false;
    }

    public void PointerUpRight()
    {
        moveRight = false;
    }

    public void PointerDownJump()
    {
        if ((isGrounded || isOnEnemyHead) && !jump)
        {
            jump = true;
            rigidBody.AddForce(new Vector2(0f, jumpForce), ForceMode2D.Impulse);
        }
    }

    public void PointerUpJump()
    {
        jump = false;
    }

    public void PointerDownClimb()
    {
        if ((isGrounded || isOnEnemyHead) && collidesWithLadder)
        {
            climb = true;
            rigidBody.AddForce(new Vector2(0f, jumpForce), ForceMode2D.Impulse);
        }
    }

    public void PointerUpClimb()
    {
        climb = false;
    }
}