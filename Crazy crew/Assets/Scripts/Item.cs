using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour
{
    CanvasController canvasController;
    public void OnCollisionEnter2D(Collision2D collision)
    {
        canvasController = new CanvasController();
        if (collision.gameObject.name == "Player")
        {
            Player player = collision.gameObject.GetComponent<Player>();
            switch (this.tag)
            {
                case "Coin":
                    player.coinsCount++;
                    break;
                case "Potion":
                    player.potionsCount++;
                    break;
            }
            Destroy(this.gameObject);
            canvasController.TextUpdate(player.coinsCount, player.potionsCount);
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
